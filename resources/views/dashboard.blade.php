<x-app-layout>
    @section('csslinks')
    <link rel="stylesheet" href="{{ '/css/general.css' }}">
    @endsection
    <div class="container-fluid maincont">
        <div class="row">
            <div class="col-md-2 offset-md-1">
                <button class="buttons" id="addBtn" data-bs-toggle="modal" data-bs-target="#addModal">Add Trip</button>
            </div> 
            <div class="col-md-2 offset-md-5">
              <form action="" method="POST">
                  @csrf
                  <div class="">
                      <input type="text" class="search-input" placeholder="Search">
                  </div>
              </form>
          </div>   
        </div>
        <div class="row tablesrow">
          <div class="col-md-10 offset-md-1">
            <div id="succesdiv" class="alert alert-success">Trip succesfuly added</div>
            <div id="succesdivdelete" class="alert alert-success">Trip succesfuly deleted</div>
             <table class="tables table table-striped" id="tab1">
                <thead id="kir">
                  <tr>
                      <th>No.</th>
                      <th>Date of Loading</th>
                      <th>Reference</th>
                      <th>Customer</th>
                      <th>From</th>
                      <th>To</th>
                      <th>Maps</th>
                      <th>Truck</th>
                      <th>Tralier</th>
                      <th>Driver</th>
                      <th>Shipments</th>
                      <th>Km</th>
                      <th>Tons</th>
                      <th></th>
                      <th></th>
                  </tr>
                </thead> 
                <tbody id="searchList"></tbody>  
                <tbody id="tab">                
                </tbody>
            </table>   
          </div> 
        </div>
    </div>
    <!-- modals -->
    @include('modals.crudmodal')
    <!-- jQuery -->
    @include('jqlink')
</x-app-layout>
