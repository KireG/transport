<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AppController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('show', [AppController::class, 'show'])->name('show');
Route::post('inserttrip', [AppController::class, 'insert'])->name('insert');
Route::delete('delete/{id}', [AppController::class, 'delete'])->name('delete');
Route::post('edit/{id}', [AppController::class, 'edit'])->name('edit');
