<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use HasFactory;

    protected $fillable = [
      'registration', 'manufacturer', 'model', 'chacy_number', 'year', 'euro_norm_id'
    ];

    public function trips(){
        return $this->hasOne(Trip::class);
    }

    public function euro_norms(){
        return $this->belongsTo(Euro_norm::class);
    }
}
