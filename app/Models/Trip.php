<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    use HasFactory;

    protected $fillable = [
     'reference', 'begin_country', 'end_country', 'route', 'driver_id', 'customer_id', 'vehicle_id', 'additional_info', 'number_of_shipments', 'kilometers', 'tons'
    ];

    public function costs(){
        return $this->hasOne(Cost::class);
    }

    public function invoices(){
        return $this->belongsToMany(Invoice::class)->withPivot('trips_invoices');
    }

    public function countries1(){
        return $this->belongsTo(Country::class, 'begin_country');
    }

    public function countries2(){
        return $this->belongsTo(Country::class, 'end_country');
    }

    public function drivers(){
        return $this->belongsTo(Trip::class, 'driver_id');
    }

    public function customers(){
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function vehicles(){
        return $this->belongsTo(Vehicle::class, 'vehicle_id');
    }
}

