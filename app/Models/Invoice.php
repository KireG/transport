<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    protected $fillable = [
     'number', 'customer_id', 'amount', 'created', 'due_date'
    ];

    public function trips(){
        return $this->belongsToMany(Trip::class)->withPivot('trips_invoices');
    }
}
