<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cost extends Model
{
    use HasFactory;

    protected $fillable = [
        'trip_id', 'drivers_cost', 'fuel', 'toll', 'additional' 
    ];

    public function trips(){
        return $this->belongsTo(Trip::class);
    }
}
