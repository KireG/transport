<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Trip;
use App\Models\Country;
use App\Models\Vehicle;
use App\Models\Driver;
use App\Models\Customer;

class AppController extends Controller
{
  // Trips_____________________________

  public function showTrips(){
      $trips = Trip::All();
      $countries = Country::All();
      $vehicles = Vehicle::ALl();
      $drivers = Driver::All();
      $customers = Customer::All();
      return view('dashboard', compact('trips','countries','vehicles', 'drivers','customers') );
  }

  public function show(){
    $trips2 = Trip::Join('countries as b', 'b.id', '=', 'trips.begin_country')
            ->leftJoin('countries as e', 'e.id', '=', 'trips.end_country')
            ->leftJoin('drivers', 'drivers.id', '=', 'trips.driver_id')
            ->leftJoin('customers', 'customers.id', '=', 'trips.customer_id')
            ->leftJoin('vehicles', 'vehicles.id', '=', 'trips.vehicle_id')
            ->select('trips.id','trips.date','trips.reference','trips.route', 'trips.additional_info','trips.number_of_shipments','trips.kilometers','trips.tons','b.name AS begin_country','e.name  AS end_country','drivers.name AS driver', 'customers.name AS customer', 'vehicles.registration')
            ->orderBy('trips.date', 'desc')
            ->get();

    return response()->json($trips2);
}

public function delete($id){
    $trip = Trip::where('id', $id)->first();
   // $trip->delete();
    if($trip){
      $trip->delete();
      return response()->json(['success']);
    }else {
      return response()->json(['error'=>'User not found'],404);
    }
}

public function insert(Request $req)
{

    // $validator = \Validator::make($req->all(),
    // [
    //     'date' => 'required',
    //     'reference' => 'required',
    //     'customer' => 'vehicle',
    //     'begin_country' => 'required',
    //     'end_country' => 'required',
    //     'route' => 'required',
    //     'vehicle' => 'required',
    //     'additional_info' => 'required',
    //     'driver' => 'required',
    //     'number_of_shipments' => 'required',
    //     'kilometers' => 'required',
    //     'tons' => 'required'
    // ]);

    // if($validator->fails())
    // {
    //     return response()->json(['errors' => $validator->messages() ], 400);
    // }


    $trip = new Trip();
    $trip->date = $req->date;
    $trip->reference = $req->reference;
    $trip->customer_id = $req->customer_id;
    $trip->begin_country = $req->begin_country;
    $trip->end_country = $req->end_country;
    $trip->route = $req->route;
    $trip->vehicle_id = $req->vehicle_id;
    $trip->additional_info = $req->additional_info;
    $trip->driver_id = $req->driver_id;
    $trip->number_of_shipments = $req->number_of_shipments;
    $trip->kilometers = $req->kilometers;
    $trip->tons = $req->tons;
    $trip->save();

    
    return response()->json($trip);
}

public function edit(Request $req, $id){

  $trip = Trip::where('id',$id)->first();
  if($trip)
  {
      $trip->update($req->all());
    
      return response()->json($trip);
  } else {
      return response()->json(['error'=>'Trip not found'],404);
  }


}

public function search(Request $request){
  $trips = Trip::where( 'reference', 'like', '%'.$request->get('searchQuery').'%')->get();
  return json_encode($trips);
} 

//Financies______________

public function financies () {
  return view('financies');
}

//Analitics______________

public function analitics () {
  return view('analitics');
}

}
