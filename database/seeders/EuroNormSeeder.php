<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EuroNormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('euro_norms')->insert([
            ['class'=>4],
            ['class'=>5],
            ['class'=>6],
        ]);
    }
}
