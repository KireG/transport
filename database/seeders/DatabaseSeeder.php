<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(EuroNormSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(CustomerSeeder::class);
        $this->call(InvoiceSeeder::class);
        $this->call(DriverSeeder::class);
        $this->call(VehicleSeeder::class);
        $this->call(TripSeeder::class);
        $this->call(CostSeeder::class);
        $this->call(Trips_InvoicesSeeder::class);
        // \App\Models\User::factory(10)->create();
    }
}
